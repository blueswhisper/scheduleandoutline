<%@ page language="java" import="java.util.*,java.sql.*,com.blueswhisper.db.DBHelper" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="conf/conf-meta.html" %>
<title>教学进度与大纲</title>
<%@ include file="conf/conf-css.jsp" %>
</head>
<body>
<a class="pull-right btn btn-primary" href="./login.jsp">跳转到登陆页面</a>
<table class="table table-hover">
<thead>
  <tr>
    <th>序号</th>
    <th>课号</th>
    <th>中文课程名</th>
    <th>开课单位</th>
    <th>学分</th>
    <th>课程大纲</th>
    <th>大纲填写老师</th>
  </tr>
</thead>
<tbody>
  <%
  	String rootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
  	String sql = "SELECT course.c_id,course.c_name_zh,"+
  	"course.unit,course.credit,outline.o_file,teacher.t_name "+
  	"FROM course,outline,teacher WHERE course.c_id = outline.c_id "+
  	"AND outline.t_id = teacher.t_id "+
  	"AND outline.o_file is not null";
  	ResultSet rs = DBHelper.executeQuery(sql);
  	int count = 0;
  	while(rs.next()) {
  		count++;
  		out.println("<tr>");
  		out.println("<td>"+count+"</td>");
  		out.println("<td>"+rs.getString(1)+"</td>");
  		out.println("<td>"+rs.getString(2)+"</td>");
  		out.println("<td>"+rs.getString(3)+"</td>");
  		out.println("<td>"+rs.getString(4)+"</td>");
  		out.println("<td><a href='"+rootPath+rs.getString(5)+"'>下载</a></td>");
  		out.println("<td>"+rs.getString(6)+"</td>");
  		out.println("</tr>");
  		
  	}
  %>
</tbody>
</table>

<%@ include file="conf/conf-copyright.html" %>
<%@ include file="conf/conf-js.jsp" %>
</body>
</html>