<%@ page language="java" import="java.util.*,com.blueswhisper.db.DBHelper,java.sql.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>上传课程大纲</title>
<%@ include file="../conf/conf-css.jsp" %>

</head>
<body>
<script type="text/javascript">
	var status = <%=request.getParameter("status")%>;
	if(status == 0) {
		window.alert("上传教学大纲失败！");
	} else if(status == 1) {
		window.alert("上传教学大纲成功！");
	} else{
		;
	}
	
</script>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-teacher.html" %>
		<%@ include file="sidebar-teacher.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			<div class="row">
				<%
				if((int)request.getSession().getAttribute("outEdit") == 0) {
					out.println("<h3>管理员未开放课程大纲上传，当前状态不可编辑教学大纲</h3>");
				} else {
					Map<String,String> map = (Map<String,String>)request.getSession().getAttribute("cIdNameMap");
					String sql = "SELECT c_id FROM outline where editable=1 AND t_id="
							+(String)request.getSession().getAttribute("user");
						ResultSet rs = DBHelper.executeQuery(sql);
						while(rs.next()) {
							out.println("<form class='form-inline' action='../addoutlinefile' method='post' enctype='multipart/form-data'>");
							out.println("<h3>上传课程大纲</h3>");
							out.println("<input type='hidden' name='role' value='teacher'>");
							out.println("<input type='hidden' name='cid' value="+rs.getString("c_id")+">");
							out.println("<p class='text-left'>课程 ： "+ map.get(rs.getString("c_id"))+"</p>");
							out.println("<div class='form-group'>");
							out.println("<label for='outFile'>上传课程大纲文档</label>");
							out.println("<input id='outFile' name='outFile' type='file' required>");
							out.println("</div>");
							out.println("<div class='form-group'>");
							out.println("<div class='col-sm-12'>");
							out.println("<input type='submit' class='btn btn-default' value='上传文档'>");
							out.println("</div>");
							out.println("</div>");
							out.println("</form>");
						}
				}
				%>
			</div>
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#sidebar ul>li:eq(2)").addClass("active");
});
</script>
</body>
</html>