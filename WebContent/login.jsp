<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="conf/conf-meta.html" %>
<title>登录教学进度与教学大纲管理平台</title>
<%@ include file="conf/conf-css.jsp" %>
<link href="css/bootstrap.css" rel="stylesheet">
<style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-image: url(res/img/back-ground.jpg);
}

.form-signin {
  max-width: 380px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .radio {
  margin-bottom: 10px;
}
.form-signin .radio {
  font-weight: normal;
  display: inline-block;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>
</head>
<body>
<div class="container">

      <form class="form-signin" action="login" method="post">
        <h2 class="form-signin-heading">登录管理系统</h2>
        <label for="user" class="sr-only">工号</label>
        <input type="text" name="user" id="user" class="form-control" placeholder="工&nbsp号" required autofocus>
        <label for="pwd" class="sr-only">密 码</label>
        <input type="password" name="pwd" id="pwd" class="form-control" placeholder="密&nbsp码" required>
        <div class="radio">
          <label>
            <input type="radio" name="type" value="1" checked> 教师
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="type" value="0"> 管理员
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
      </form>

    </div> <!-- /container -->
    <%@ include file="conf/conf-copyright.html" %>
</body>
</html>