<%@ page language="java" import="com.blueswhisper.db.DBHelper,java.util.*,java.sql.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>分配大纲填写情况</title>
<%@ include file="../conf/conf-css.jsp" %>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-admin.html" %>
		<%@ include file="sidebar-admin.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			<div class="row">
			<%
				if((int)request.getSession().getAttribute("outEdit") == 0) {
					out.println("<h3>请先打开课程大纲填写开关，并注销后重新登录. 当前状态不可编辑课程大纲</h3>");
				} else {
					out.println("<table class='table table-hover'>");
					out.println("<thead>");
					out.println("<tr>");
					out.println("<th>#</th>");
					out.println("<th>课号</th>");
					out.println("<th>中文名称</th>");
					out.println("<th>开课单位</th>");
					out.println("<th>大纲填写老师</th>");
					out.println("<th>保存</th>");
					out.println("</tr>");
					out.println("</thead>");
					out.println("<tbody>");
					Map<String,String> map = (Map<String,String>)request.getSession().getAttribute("tIdNameMap");
					String sql = "SELECT c_id,c_name_zh,unit FROM course";
					ResultSet rs = DBHelper.executeQuery(sql);
					int count = 0;
					while(rs.next()) {
						out.println("<tr>");
						count++;
						out.println("<td>"+ count +"</td>");
						out.println("<td>"+ rs.getString("c_id")+"</td>");
						out.println("<td>"+ rs.getString("c_name_zh")+"</td>");
						out.println("<td>"+ rs.getString("unit")+"</td>");
						out.println("<td>");
						out.println("<select id='tId' name='tId'>");
						Map.Entry entry = null;
						Iterator it = map.entrySet().iterator();
						while(it.hasNext()) {
							entry = (Map.Entry)it.next();
							out.println("<option value='"+entry.getKey()+"'>"+entry.getValue()+"</option>");
						}
						out.println("</select>");
						out.println("</td>");
						out.println("<td>");
						out.println("<button class='save btn btn-primary'>保存</button>");
						out.println("</td>");
						out.println("</tr>");
					}
					out.println("</tbody>");
					out.println("</table>");
				}
				%>
			</div>
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#subOutMenu").addClass("in");
	$("#subOutMenu>li:eq(1)").addClass("active");
	$('.save').click(function(){
		var cid = $(this).parent().parent().find("td:eq(1)").text();
		var tid = $(this).parent().parent().find("td:eq(4)>select").val();
		$.post("../allocateoutline",{c_id:cid,t_id:tid},function(data,status){
			;
		});
	});
});
</script>
</body>
</html>