<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>修改课程信息</title>
<%@ include file="../conf/conf-css.jsp" %>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-admin.html" %>
		<%@ include file="sidebar-admin.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			身份类型: 管理员 当前用户工号: ${sessionScope.user}
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#subCouMenu").addClass("in");
	$("#subCouMenu>li:eq(1)").addClass("active");
});
</script>
</body>
</html>