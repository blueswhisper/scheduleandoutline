<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>课程大纲填写情况</title>
<%@ include file="../conf/conf-css.jsp" %>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-admin.html" %>
		<%@ include file="sidebar-admin.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			身份类型: 管理员 当前用户工号: ${sessionScope.user}
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#subOutMenu").addClass("in");
	$("#subOutMenu>li:eq(0)").addClass("active");
});
</script>
</body>
</html>