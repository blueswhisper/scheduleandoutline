<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>开关进度填写</title>
<%@ include file="../conf/conf-css.jsp" %>
<link href="../css/bootstrap-switch.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-admin.html" %>
		<%@ include file="sidebar-admin.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			<div class="row">
				<h3>教学进度填写启动开关</h3>
				<%
				if((int)request.getSession().getAttribute("schEdit") == 1) {
					out.println("<input type='checkbox' id='scheduleSwitcher' name='scheduleSwitcher' checked>");
				} else {
					out.println("<input type='checkbox' id='scheduleSwitcher' name='scheduleSwitcher'>");
				}
				%>
			</div>
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script src="../js/bootstrap-switch.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#sidebar ul>li:eq(1)>ul").addClass("in");
	$("#sidebar ul>li:eq(1)>ul>li:eq(5)").addClass("active");
	$("#scheduleSwitcher").bootstrapSwitch()
	$("#scheduleSwitcher").on('switchChange.bootstrapSwitch', function(event, state) {
		$.post("../switcher",{src:this.name,status:state},function(data,status){
			;
		});
	});
});
</script>
</body>
</html>