<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>开关课程大纲填写</title>
<%@ include file="../conf/conf-css.jsp" %>
<link href="../css/bootstrap-switch.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-admin.html" %>
		<%@ include file="sidebar-admin.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			<div class="row">
				<h3>课程大纲上传启动开关</h3>
				<%
				if((int)request.getSession().getAttribute("outEdit") == 1) {
					out.println("<input type='checkbox' id='outlineSwitcher' name='outlineSwitcher' checked>");
				} else {
					out.println("<input type='checkbox' id='outlineSwitcher' name='outlineSwitcher'>");
				}
				%>
			</div>
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script src="../js/bootstrap-switch.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#subOutMenu").addClass("in");
	$("#subOutMenu>li:eq(3)").addClass("active");
	$("#outlineSwitcher").bootstrapSwitch()
	$("#outlineSwitcher").on('switchChange.bootstrapSwitch', function(event, state) {
		$.post("../switcher",{src:this.name,status:state},function(data,status){
			;
		});
	});
});
</script>
</body>
</html>