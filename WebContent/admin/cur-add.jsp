<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../conf/conf-meta.html" %>
<title>新增开课信息</title>
<%@ include file="../conf/conf-css.jsp" %>
</head>
<body>
<script type="text/javascript">
	var status = <%=request.getParameter("status")%>;
	if(status == 0) {
		window.alert("添加开课信息失败！");
	} else if(status == 1) {
		window.alert("一条开课信息已添加");
	} else if(status == 2){
		window.alert("批量添加开课信息成功");
	}
	
</script>
<div class="container-fluid">
	<div class="row">
		<%@ include file="header-admin.html" %>
		<%@ include file="sidebar-admin.html" %>
		
		<!-- content-wrap -->
		<div id="content" class="col-md-10">
			<div class="row">
				<div  class="col-md-6">
					<form class="form-horizontal" action="../addcurriculum" method="get">
					<h3 class="text-center">添加单条开课信息</h3>
					<div class="form-group">
					<label for="courseId" class="col-sm-2 control-label">课号</label>
					<div class="col-sm-10">
						<select id="courseId" name="courseId" class="form-control">
						<%
						Map<String,String> cMap = (Map<String,String>)request.getSession().getAttribute("cIdNameMap");
						Map.Entry cEntry = null;
						Iterator cIterator = cMap.entrySet().iterator();
						while(cIterator.hasNext()) {
							cEntry = (Map.Entry)cIterator.next();
							out.println("<option value='"+cEntry.getKey()+"'>"+cEntry.getKey()+"\t"+cEntry.getValue()+"</option>");
						}
						%>
						</select>
					</div>
					</div>
					<div class="form-group">
					<label for="teacherId" class="col-sm-2 control-label">任课教师</label>
					<div class="col-sm-10">
						<select id="teacherId" name="teacherId" class="form-control">
						<%
						
						Map<String,String> tMap = (Map<String,String>)request.getSession().getAttribute("tIdNameMap");
						Map.Entry tEntry = null;
						Iterator tIterator = tMap.entrySet().iterator();
						while(tIterator.hasNext()) {
							tEntry = (Map.Entry)tIterator.next();
							out.println("<option value='"+tEntry.getKey()+"'>"+tEntry.getValue()+"</option>");
						}
						%>
						</select>
					</div>
					</div>
					<div class="form-group">
					<label for="courseIdOrder" class="col-sm-2 control-label">开课号</label>
					<div class="col-sm-10">
						<input id="courseIdOrder" name="courseIdOrder" type="text" class="form-control" required placeholder="共9位，前7位与课号相同，后2位自定">
					</div>
					</div>
					<div class="form-group">
					<label for="courseClass" class="col-sm-2 control-label">课程类别</label>
					<div class="col-sm-10">
						<select id="courseClass" name="courseClass" class="form-control">
						<option value="公共必修课程">公共必修课程</option>
						<option value="跨学科类(任选)">跨学科类(任选)</option>
						<option value="实践课程(必修)">实践课程(必修)</option>
						<option value="通识教育课(必修)">通识教育课(必修)</option>
						<option value="学科基础课(必修)">学科基础课(必修)</option>
						<option value="学科基础课(限选)">学科基础课(限选)</option>
						<option value="专业课(必修)">专业课(必修)</option>
						<option value="专业类(公共类限选)">专业类(公共类限选)</option>
						<option value="专业类(限选)">专业类(限选)</option>
						</select>
					</div>
					</div>
					<div class="form-group">
					<label for="review" class="col-sm-2 control-label">考查方式</label>
					<div class="col-sm-10">
						<select id="review" name="review" class="form-control">
						<option value="考查">考查</option>
						<option value="考试">考试</option>
						</select>
					</div>
					</div>
					<div class="form-group">
					<label for="place" class="col-sm-2 control-label">上课地点</label>
					<div class="col-sm-10">
						<input id="place" name="place" type="text" class="form-control" placeholder="例如：南1101，第三机房">
					</div>
					</div>
					<div class="form-group">
					<label for="weekday" class="col-sm-2 control-label">星期</label>
					<div class="col-sm-10">
						<select id="weekday" name="weekday" class="form-control">
						<option value="一">一</option>
						<option value="二">二</option>
						<option value="三">三</option>
						<option value="四">四</option>
						<option value="五">五</option>
						<option value="六">六</option>
						<option value="日">日</option>
						<option value="">其他</option>
						</select>
					</div>
					</div>
					<div class="form-group">
					<label for="sectionOrder" class="col-sm-2 control-label">节次</label>
					<div class="col-sm-10">
						<input id="sectionOrder" name="sectionOrder" type="text" class="form-control" placeholder="例如：3,4;9,10,11">
					</div>
					</div>
					<div class="form-group">
					<label for="week" class="col-sm-2 control-label">上课周</label>
					<div class="col-sm-10">
						<input id="week" name="week" type="text" class="form-control" placeholder="例如：d,s,q,4-9">
					</div>
					</div>
					<div class="form-group">
					<label for="reAttend" class="col-sm-2 control-label">重修</label>
					<div class="col-sm-10">
						<select id="reAttend" name="reAttend" class="form-control">
						<option value=0>否</option>
						<option value=1>是</option>
						</select>
					</div>
					</div>
					<div class="form-group">
					<label for="district" class="col-sm-2 control-label">校区</label>
					<div class="col-sm-10">
						<select id="district" name="district" class="form-control">
						<option value="浦东校区">浦东校区</option>
						<option value="杨浦校区">杨浦校区</option>
						</select>
					</div>
					</div>
					<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<input name="submit" type="submit" class="btn btn-default" value="提交课程信息">
					</div>
					</div>
					</form>
				</div>
				<div class="col-md-6">
					<form class="form-inline" action="../addcurriculum" method="post" enctype="multipart/form-data">
					<h3>批量导入开课信息</h3>
					<input type="hidden" name="role" value="admin">
					<div class="form-group">
						<label for="curFile">上传开课信息表</label>
						<input id="curFile" name="curFile" type="file" required>
					</div>
					<div class="form-group">
					<div class="col-sm-12">
						<input name="upload" type="submit" class="btn btn-default" value="导入开课信息">
					</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		
		<%@ include file="../conf/conf-copyright.html" %>
	</div>
</div>
<%@ include file="../conf/conf-js.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#sidebar ul>li:eq(1)>ul").addClass("in");
	$("#sidebar ul>li:eq(1)>ul>li:eq(1)").addClass("active");
});
</script>
</body>
</html>