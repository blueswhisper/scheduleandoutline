package com.blueswhisper.pojo;

/**
 * 数据库教学进度表的实体映射类
 * @author Blues Whisper
 *
 */
public class Schedule {

	private String courseIdOrder;               //开课号
	private String content;                     //教学进度的XML文本内容
	private boolean editale;                    //教学进度是否可编辑
	
	public Schedule(String courseIdOrder, String content) {
		super();
		this.courseIdOrder = courseIdOrder;
		this.content = content;
	}
	
	public Schedule(String courseIdOrder, String content, boolean editale) {
		super();
		this.courseIdOrder = courseIdOrder;
		this.content = content;
		this.editale = editale;
	}
	
	public String getCourseIdOrder() {
		return courseIdOrder;
	}
	public void setCourseIdOrder(String courseIdOrder) {
		this.courseIdOrder = courseIdOrder;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isEditale() {
		return editale;
	}
	public void setEditale(boolean editale) {
		this.editale = editale;
	}
}
