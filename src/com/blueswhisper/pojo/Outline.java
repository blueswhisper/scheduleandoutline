package com.blueswhisper.pojo;

/**
 * 数据库中教学大纲表的实体映射类
 * @author Blues Whisper
 *
 */
public class Outline {

	private String courseId;               //课号
	private String teacherId;              //上传大纲的教师工号
	private String outlineFile;            //大纲文件所在的路径
	private boolean editable;              //大纲是否可编辑
	
	public Outline(String courseId, String teacherId, String outlineFile) {
		super();
		this.courseId = courseId;
		this.teacherId = teacherId;
		this.outlineFile = outlineFile;
	}
	
	public Outline(String courseId, String teacherId, String outlineFile,
			boolean editable) {
		super();
		this.courseId = courseId;
		this.teacherId = teacherId;
		this.outlineFile = outlineFile;
		this.editable = editable;
	}
	
	public Outline(String courseId, String teacherId) {
		this.courseId = courseId;
		this.teacherId = teacherId;
	}

	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getOutlineFile() {
		return outlineFile;
	}
	public void setOutlineFile(String outlineFile) {
		this.outlineFile = outlineFile;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
}
