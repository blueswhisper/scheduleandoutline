package com.blueswhisper.pojo;

/**
 * 数据库中开课表的实体映射类
 * @author Blues Whisper
 *
 */
public class Curriculum {

	private String courseId;                  //课号
	private String teacherId;                 //任课教师工号
	private String courseIdOrder;             //开课号
	private String courseClass;               //课程类别
	private String review;                    //考查方式
	private String place;                     //上课地点
	private String weekday;                   //上课时间-星期几
	private String sectionOrder;               //上课时间-节次
	private String week;                      //上课时间-周数
	private int reAttend;                 	  //是否重修
	private String district;                  //上课校区
	public Curriculum(String courseId, String teacherId, String courseIdOrder,
			String courseClass, String review, String place, String weekday,
			String sectionOrder, String week, int reAttend, String district) {
		super();
		this.courseId = courseId;
		this.teacherId = teacherId;
		this.courseIdOrder = courseIdOrder;
		this.courseClass = courseClass;
		this.review = review;
		this.place = place;
		this.weekday = weekday;
		this.sectionOrder = sectionOrder;
		this.week = week;
		this.reAttend = reAttend;
		this.district = district;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getCourseIdOrder() {
		return courseIdOrder;
	}
	public void setCourseIdOrder(String courseIdOrder) {
		this.courseIdOrder = courseIdOrder;
	}
	public String getCourseClass() {
		return courseClass;
	}
	public void setCourseClass(String courseClass) {
		this.courseClass = courseClass;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getWeekday() {
		return weekday;
	}
	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}
	public String getSectionOrder() {
		return sectionOrder;
	}
	public void setSectionOrder(String sectionOrder) {
		this.sectionOrder = sectionOrder;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public int getReAttend() {
		return reAttend;
	}
	public void setReAttend(int reAttend) {
		this.reAttend = reAttend;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	@Override
	public String toString() {
		return "Curriculum [courseId=" + courseId + ", teacherId=" + teacherId
				+ ", courseIdOrder=" + courseIdOrder + ", courseClass="
				+ courseClass + ", review=" + review + ", place=" + place
				+ ", weekday=" + weekday + ", sectionOrder=" + sectionOrder
				+ ", week=" + week + ", reAttend=" + reAttend + ", district="
				+ district + "]";
	}
	
}
