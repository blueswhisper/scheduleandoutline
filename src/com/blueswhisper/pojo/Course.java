package com.blueswhisper.pojo;

/**
 * 数据库中课程表的实体映射类
 * @author Blues Whisper
 *
 */
public class Course {

	private String courseId;                 //课号
	private String courseNameZh;             //中文名
	private String courseNameEn;             //英文名
	private String unit;                     //开课单位
	private double credit;                   //学分
	private int periodAll;                   //总学时
	private int periodTheory;                //理论学时
	private int periodTest;                  //实践学时
	
	public Course(String courseId, String courseNameZh, String courseNameEn,
			String unit, double credit, int periodAll, int periodTheory,
			int periodTest) {
		super();
		this.courseId = courseId;
		this.courseNameZh = courseNameZh;
		this.courseNameEn = courseNameEn;
		this.unit = unit;
		this.credit = credit;
		this.periodAll = periodAll;
		this.periodTheory = periodTheory;
		this.periodTest = periodTest;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseNameZh() {
		return courseNameZh;
	}

	public void setCourseNameZh(String courseNameZh) {
		this.courseNameZh = courseNameZh;
	}

	public String getCourseNameEn() {
		return courseNameEn;
	}

	public void setCourseNameEn(String courseNameEn) {
		this.courseNameEn = courseNameEn;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public int getPeriodAll() {
		return periodAll;
	}

	public void setPeriodAll(int periodAll) {
		this.periodAll = periodAll;
	}

	public int getPeriodTheory() {
		return periodTheory;
	}

	public void setPeriodTheory(int periodTheory) {
		this.periodTheory = periodTheory;
	}

	public int getPeriodTest() {
		return periodTest;
	}

	public void setPeriodTest(int periodTest) {
		this.periodTest = periodTest;
	}
	
}
