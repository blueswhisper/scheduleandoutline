package com.blueswhisper.dao;

import com.blueswhisper.db.DBHelper;
import com.blueswhisper.pojo.Outline;

/**
 * Outline实体映射的操纵类
 * @author Blues Whisper
 * @Time 2015年1月15日
 */
public class OutlineDAO {

	/**
	 * 将分配后的结果写入数据库
	 * @param ol Outline对象
	 * @return
	 */
	public int updateOutline(Outline ol) {
		String sql = "UPDATE outline SET t_id="+ol.getTeacherId()+" WHERE c_id="+ol.getCourseId();
		System.out.println(sql);
		return DBHelper.executeUpdate(sql);
	}
}
