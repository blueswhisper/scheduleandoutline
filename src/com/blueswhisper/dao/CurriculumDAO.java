package com.blueswhisper.dao;

import java.util.List;

import com.blueswhisper.db.DBHelper;
import com.blueswhisper.pojo.Curriculum;

/**
 * 开课信息的实体操作类
 * @author Blues Whisper
 *
 */
public class CurriculumDAO {
	
	/**
	 * 插入单条开课信息
	 * @param cur 开课信息对象
	 * @return 插入操作执行后的状态码
	 */
	public int addCurriculum(Curriculum cur) {
		String courseId = cur.getCourseId(); 
		String teacherId = cur.getTeacherId();
		String courseIdOrder = cur.getCourseIdOrder();
		String courseClass = cur.getCourseClass();
		String review = cur.getReview();
		String place = cur.getPlace();
		String weekday = cur.getWeekday();
		String sectionOrder = cur.getSectionOrder();
		String week = cur.getWeek();
		int reAttend = cur.getReAttend();
		String district = cur.getDistrict();
		
		String sql = "INSERT INTO curriculum(c_id,t_id,c_id_order,"
				+ "c_class,review,place,weekday,section_order,week,"
				+ "re_attend,district) VALUES ('"+courseId+"','"+teacherId
				+"','"+courseIdOrder+"','"+courseClass+"','"+review
				+"','"+place+"','"+weekday+"','"+sectionOrder+"','"+week
				+"',"+reAttend+",'"+district+"')";
		return DBHelper.executeUpdate(sql);
	}
	
	/**
	 * 批量插入开课信息
	 * @param curs 开课信息实体类的list表
	 */
	public void addCurriculums(List<Curriculum> curs) {
		DBHelper.insertBatched(curs);
		DBHelper.closeConnection();
	}
}
