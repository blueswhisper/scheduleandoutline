package com.blueswhisper.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.blueswhisper.db.DBHelper;
import com.blueswhisper.pojo.Curriculum;

/**
 * Excel文件解析类
 * @author Blues Whisper
 * @Time 2015年1月15日
 */
public class ExcelParser {
	
	/**
	 * 提供对外调用的主要方法
	 * @param filePath 要读取的excel文档的路径，包含文件名
	 * @return Curriculum实体类的List
	 * @throws Exception
	 */
	public static List<Curriculum> readExcel(String filePath) throws Exception{
		//获取文件输入流
		InputStream fis = new FileInputStream(filePath);
		//处理excel2007前后的文件类型
		boolean after2007 = false;
		if(filePath.toLowerCase().endsWith("xlsx")) {
			after2007 = true;
		}
		Workbook wb = null;
		if(after2007) {
			wb = new XSSFWorkbook(fis);
		} else {
			wb = new HSSFWorkbook(fis);
		}
		//获取第一个工作簿
		Sheet sheet = wb.getSheetAt(0);
		Map<String,String> map = getTeacherMap();
		List<Curriculum> curs = new ArrayList<Curriculum>();
		for(int i = (sheet.getFirstRowNum()+1); i < sheet.getPhysicalNumberOfRows(); i++) {
			Row row = sheet.getRow(i);
			Cell cell = row.getCell(0);
			if(cell.getCellType() != Cell.CELL_TYPE_STRING) {
				cell.setCellType(Cell.CELL_TYPE_STRING);
			}
			String courseId = cell.getStringCellValue();
			cell = row.getCell(1);
			if(cell.getCellType() != Cell.CELL_TYPE_STRING) {
				cell.setCellType(Cell.CELL_TYPE_STRING);
			}
			String courseIdOrder = cell.getStringCellValue();
			cell = row.getCell(3);
			String teacherId = map.get(cell.getStringCellValue());
			cell = row.getCell(4);
			String courseClass = cell.getStringCellValue();
			cell = row.getCell(5);
			String review = cell.getStringCellValue();
			cell = row.getCell(6);
			String place = cell.getStringCellValue();
			cell = row.getCell(7);
			String weekday = cell.getStringCellValue();
			cell = row.getCell(8);
			String sectionOrder = cell.getStringCellValue();
			cell = row.getCell(9);
			String week = cell.getStringCellValue();
			cell = row.getCell(10);
			int reAttend = cell.getStringCellValue().equals("否")? 0 : 1;
			cell = row.getCell(11);
			String district = cell.getStringCellValue();
			Curriculum cur = new Curriculum(courseId, teacherId, 
					courseIdOrder, courseClass, review, place, weekday, 
					sectionOrder, week, reAttend, district);
			curs.add(cur);
		}
		wb.close();
		fis.close();
		return curs;
	}
	
	//私有方法-获取教师姓名-工号的键值对
	private static Map<String,String> getTeacherMap() {
		Map<String,String> map = new HashMap<String,String>();
		String sql = "SELECT t_name,t_id FROM teacher";
		ResultSet rs = DBHelper.executeQuery(sql);
		try {
			while(rs.next()) {
				map.put(rs.getString(1), rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}

}
