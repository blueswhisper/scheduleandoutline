package com.blueswhisper.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;

/**
 * 文件上传辅助类
 * @author Blues Whisper
 * @Time 2015年1月15日
 */
public class UploadUtil {

	private ServletConfig servletConfig;
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	public UploadUtil(ServletConfig servletConfig, HttpServletRequest request,
			HttpServletResponse response) {
		this.servletConfig = servletConfig;
		this.request = request;
		this.response = response;
		File fp = new File("/upload");
		if(!fp.exists()) {
			fp.mkdir();
		}
	}

	public String upload() {
		String filePath = null;
		SmartUpload su = new SmartUpload();
		filePath = null;
		try {
			//初始化上下文信息
			su.initialize(servletConfig, request, response);
			//设定文件类型限制
			su.setAllowedFilesList("xls,xlsx");
			su.setDeniedFilesList("exe,jsp,html");
			//上传文件
			su.upload();
			com.jspsmart.upload.File file = su.getFiles().getFile(0);
			//获取隐藏参数值
			String role = su.getRequest().getParameter("role");
			//如果上传目录不存在，则创建
			File fp = new File("/upload/" + role);
			if(!fp.exists()) {
				fp.mkdir();
			}
			//将上传文件重命名
			String fName = request.getSession().getAttribute("user")+ "_" + 
			new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date())+
			"." +file.getFileExt();
			filePath = "/upload/" + role + "/" + fName;
			file.saveAs(filePath, SmartUpload.SAVE_VIRTUAL);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (SmartUploadException e) {
			e.printStackTrace();
		}
		return filePath;
	}
	
	public String[] uploadForTeacher() {
		String filePath = null;
		String[] strArr = new String[2];
		SmartUpload su = new SmartUpload();
		filePath = null;
		try {
			//初始化上下文信息
			su.initialize(servletConfig, request, response);
			//设定文件类型限制
			su.setAllowedFilesList("doc,docx,pdf");
			su.setDeniedFilesList("exe,jsp,html");
			//上传文件
			su.upload();
			com.jspsmart.upload.File file = su.getFiles().getFile(0);
			//获取隐藏参数值
			String role = su.getRequest().getParameter("role");
			String cid = su.getRequest().getParameter("cid");
			//如果上传目录不存在，则创建
			File fp = new File("/upload/" + role);
			if(!fp.exists()) {
				fp.mkdir();
			}
			//将上传文件重命名
			String fName = request.getSession().getAttribute("user")+ "_" + 
			new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date())+
			"." +file.getFileExt();
			filePath = "/upload/" + role + "/" + fName;
			file.saveAs(filePath, SmartUpload.SAVE_VIRTUAL);
			strArr[0] = filePath;
			strArr[1] = cid;
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (SmartUploadException e) {
			e.printStackTrace();
		}
		
		return strArr;
	}
	
}
