package com.blueswhisper.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 用于MD5信息摘要算法，加密字符串的辅助类
 * @author Blues Whisper
 * 
 */
public class Md5Util {
	private final static String[] hexDigits = 
		{"0","1","2","3","4","5","6","7","8",
		"9","a","b","c","d","e","f"};
	
	private static String byte2String(byte[] bytes) {
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			sBuffer.append(byte2ArrayString(bytes[i]));
			
		}
		return sBuffer.toString();
	}
	
	private static String byte2ArrayString(byte b) {
		int iRet = b;
		if(b < 0) {
			iRet +=256;
		}
		int iD1 = iRet/16;
		int iD2 = iRet%16;
		return hexDigits[iD1]+hexDigits[iD2];
	}
	
	/**
	 * 获取MD5加密后的字符串（32位版）
	 * @param encryptStr 待加密的字符串
	 * @return 加密后字节对应的字符串
	 */
	public static String getMD5(String encryptStr) {
		String resultString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byte2String(md.digest(encryptStr.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return resultString;
	}
	
}
