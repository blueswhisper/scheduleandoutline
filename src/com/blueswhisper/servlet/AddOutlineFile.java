package com.blueswhisper.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blueswhisper.dao.CurriculumDAO;
import com.blueswhisper.db.DBHelper;
import com.blueswhisper.util.ExcelParser;
import com.blueswhisper.util.UploadUtil;

/**
 * Servlet implementation class AddOutlineFile
 */
@WebServlet(description = "接收上传的课程大纲文件", urlPatterns = { "/addoutlinefile" })
public class AddOutlineFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddOutlineFile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] strArr = new UploadUtil(this.getServletConfig(),request,response).uploadForTeacher();
		String filePath = strArr[0];
		String cid = strArr[1];
		if(filePath != null) {
			String sql = "UPDATE outline SET o_file='"+filePath+"' WHERE c_id="+cid;
			System.out.println(sql);
			DBHelper.executeUpdate(sql);
			response.sendRedirect("teacher/outline.jsp?status=1");
		} else {
			response.sendRedirect("teacher/outline.jsp?status=0");
		}
	}

}
