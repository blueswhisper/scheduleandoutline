package com.blueswhisper.servlet;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blueswhisper.db.DBHelper;
import com.blueswhisper.util.Md5Util;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "登录验证", urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("user");
		String pwd = Md5Util.getMD5(request.getParameter("pwd"));
		int type = Integer.parseInt(request.getParameter("type"));
		int status = 1;
		String sql = "SELECT 1 FROM login where t_id = '" + user +"' AND password = '" + pwd + "' AND role = "+ type;
		ResultSet rs = DBHelper.executeQuery(sql);
		try {
			if(rs.next()) {
				status = rs.getInt(1);
				if(status == 1) {
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("type", type);
					initSessionChain(request);
					switch(type) {
					case 1:
						response.sendRedirect("teacher/index.jsp");
						break;
					case 0:
						response.sendRedirect("admin/index.jsp");
						break;
					default:
						response.sendRedirect("res/exception/error-login.jsp");
						break;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 向session串中添加一些初始化的键值对
	 * @param request
	 */
	private void initSessionChain(HttpServletRequest request) {
		//添加课号和中文名称对应的键值对到session串
		Map<String,String> map0 = new HashMap<String,String>();
		String sql0 = "SELECT c_id,c_name_zh FROM course";
		ResultSet rs0 = DBHelper.executeQuery(sql0);
		try {
			while(rs0.next()) {
				map0.put(rs0.getString(1), rs0.getString(2));
			}
			rs0.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getSession().setAttribute("cIdNameMap", map0);
			
		//添加教师工号和名字对应的键值对到session串
		Map<String,String> map1 = new HashMap<String,String>();
		String sql1 = "SELECT t_id,t_name FROM teacher";
		ResultSet rs1 = DBHelper.executeQuery(sql1);
		try {
			while(rs1.next()) {
				map1.put(rs1.getString(1), rs1.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getSession().setAttribute("tIdNameMap", map1);
		//初始化查询教学进度表是否可编辑
		String sqlSchedule = "SELECT DISTINCT editable FROM schedule";
		ResultSet rsSchedule = DBHelper.executeQuery(sqlSchedule);
		int rsSchValue = 0;
		int countSchedule = 0;
		try {
			while(rsSchedule.next()) {
				countSchedule++;
				rsSchValue = rsSchedule.getInt(1);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if(countSchedule == 1) {
			request.getSession().setAttribute("schEdit", rsSchValue);
		} else {
			request.getSession().setAttribute("schEdit", 1);
		}
		////初始化查询教学大纲表是否可编辑
		String sqlOutline = "SELECT DISTINCT editable FROM outline";
		ResultSet rsOutline = DBHelper.executeQuery(sqlOutline);
		try {
			
			int rsOutValue = 0;
			int countOutline = 0;
			while(rsOutline.next()) {
				countOutline++;
				rsOutValue = rsOutline.getInt(1);
			}
			if(countOutline == 1) {
				request.getSession().setAttribute("outEdit", rsOutValue);
			} else {
				request.getSession().setAttribute("outEdit", 1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
