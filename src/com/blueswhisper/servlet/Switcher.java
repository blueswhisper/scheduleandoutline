package com.blueswhisper.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blueswhisper.db.DBHelper;

/**
 * Servlet implementation class Switcher
 */
@WebServlet(description = "控制教学进度和大纲填写的开关", urlPatterns = { "/switcher" })
public class Switcher extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Switcher() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String src = request.getParameter("src");
		boolean status = Boolean.parseBoolean(request.getParameter("status"));
		String sql = new String();
		String sqlInit = new String();
		if(src.equals("scheduleSwitcher")) {
			sqlInit = "REPLACE INTO schedule(c_id_order) "
					+ "SELECT DISTINCT c_id_order FROM curriculum";
			if(status) {
				sql = "UPDATE schedule SET editable = 1";
			} else {
				sql = "UPDATE schedule SET editable = 0";
			}
		} else {
			sqlInit = "REPLACE INTO outline(c_id) "
					+ "SELECT DISTINCT c_id FROM course";
			if(status) {
				sql = "UPDATE outline SET editable = 1";
			} else {
				sql = "UPDATE outline SET editable = 0";
			}
		}
		DBHelper.executeQuery(sqlInit);
		DBHelper.executeUpdate(sql);
	}

}
