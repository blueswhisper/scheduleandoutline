package com.blueswhisper.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blueswhisper.dao.OutlineDAO;
import com.blueswhisper.pojo.Outline;

/**
 * Servlet implementation class AllocateOutline
 */
@WebServlet(description = "保存分配大纲时的ajax后台", urlPatterns = { "/allocateoutline" })
public class AllocateOutline extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllocateOutline() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String c_id = request.getParameter("c_id");
		String t_id = request.getParameter("t_id");
		Outline ol = new Outline(c_id, t_id);
		new OutlineDAO().updateOutline(ol);
	}

}
