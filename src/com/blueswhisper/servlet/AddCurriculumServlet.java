package com.blueswhisper.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blueswhisper.dao.CurriculumDAO;
import com.blueswhisper.pojo.Curriculum;
import com.blueswhisper.util.ExcelParser;
import com.blueswhisper.util.UploadUtil;

/**
 * Servlet implementation class AddCurriculumServlet
 */
@WebServlet(description = "添加单条开课信息", urlPatterns = { "/addcurriculum" })
public class AddCurriculumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCurriculumServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * 以get方式接收一条开课信息
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String courseId = request.getParameter("courseId");
		String teacherId = request.getParameter("teacherId");
		String courseIdOrder = request.getParameter("courseIdOrder");
		String courseClass = request.getParameter("courseClass");
		String review = request.getParameter("review");
		String place = request.getParameter("place");
		String weekday = request.getParameter("weekday");
		String sectionOrder = request.getParameter("sectionOrder");
		String week = request.getParameter("week");
		int reAttend = Integer.parseInt(request.getParameter("reAttend"));
		String district = request.getParameter("district");
		Curriculum cur = new Curriculum(courseId, teacherId, courseIdOrder, courseClass, review, place, weekday, sectionOrder, week, reAttend, district);
		CurriculumDAO curDao = new CurriculumDAO();
		int statusCode = curDao.addCurriculum(cur);
		if(statusCode == 1) {
			response.sendRedirect("admin/cur-add.jsp?status=1");
		} else {
			response.sendRedirect("admin/cur-add.jsp?status=0");
		}
	}

	/**
	 * 以post方式接收一个xls或xlsx文件，并读取文件内容，批量插入开课信息
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String filePath = new UploadUtil(this.getServletConfig(),request,response).upload();
		String path = getServletContext().getRealPath("/");
		String excelPath = path + filePath;
		if(filePath != null) {
			try {
				new CurriculumDAO().addCurriculums(ExcelParser.readExcel(excelPath));
			} catch (Exception e) {
				e.printStackTrace();
			}
			response.sendRedirect("admin/cur-add.jsp?status=2");
		} else {
			response.sendRedirect("admin/cur-add.jsp?status=0");
		}
	}

}
