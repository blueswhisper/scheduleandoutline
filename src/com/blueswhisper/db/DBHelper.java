package com.blueswhisper.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.blueswhisper.pojo.Curriculum;

/**
 * JDBC方式连接mysql数据库的辅助类
 * @author Blues Whisper
 *
 */
public class DBHelper {
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/sao";
	private static String user = "root", pwd = "188211422";
	private static Connection con = null;
	
	//加载外部数据库驱动
	static {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("找不到驱动类！");
			e.printStackTrace();
		}
	}
	
	/**
	 * 执行插入，删除，更新等操作
	 * 返回值大于等于1时代表受影响的行数
	 * 返回值等于0时代表执行失败（不影响任何行）
	 * @param sql 要执行操作的sql语句
	 * @return 执行后的结果
	 */
	public static int executeUpdate(String sql) {
		int statusCode = 0;
		try {
			con = DriverManager.getConnection(url, user, pwd);
			Statement cmd = con.createStatement();
			statusCode = cmd.executeUpdate(sql);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statusCode;
	}
	
	/**
	 * 批量插入开课信息到开课信息表，记得调用后，接着调用closeConnection()
	 * @param curs 开课信息实体类的list表
	 */
	public static void insertBatched(List<Curriculum> curs) {
		String sql = "INSERT INTO curriculum(c_id,t_id,c_id_order,"
				+ "c_class,review,place,weekday,section_order,week,"
				+ "re_attend,district) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		try {
			con = DriverManager.getConnection(url, user, pwd);
			con.setAutoCommit(false);
			PreparedStatement psts = con.prepareStatement(sql);
			for (Curriculum curriculum : curs) {
				psts.setString(1, curriculum.getCourseId());
				psts.setString(2, curriculum.getTeacherId());
				psts.setString(3, curriculum.getCourseIdOrder());
				psts.setString(4, curriculum.getCourseClass());
				psts.setString(5, curriculum.getReview());
				psts.setString(6, curriculum.getPlace());
				psts.setString(7, curriculum.getWeekday());
				psts.setString(8, curriculum.getSectionOrder());
				psts.setString(9, curriculum.getWeek());
				psts.setInt(10, curriculum.getReAttend());
				psts.setString(11, curriculum.getDistrict());
				psts.addBatch();
			}
			psts.executeBatch();
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 执行查询操作，返回查询后的结果集
	 * @param sql 要执行操作的sql语句
	 * @return 结果集
	 */
	public static ResultSet executeQuery(String sql) {
		ResultSet rs = null;
		try {
			con = DriverManager.getConnection(url, user, pwd);
			Statement cmd = con.createStatement();
			rs = cmd.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	/**
	 * 关闭数据库连接，主要与查询操作对应
	 */
	public static void closeConnection() {
		try {
			if(con != null && !con.isClosed()) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
